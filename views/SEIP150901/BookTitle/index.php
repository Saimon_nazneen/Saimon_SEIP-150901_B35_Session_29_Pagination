<?php
require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;
use App\Message\Message;
$objBookTitle = new BookTitle();
$allData = $objBookTitle ->index("obj");


################## search  block1 start ##################
if(isset($_REQUEST['search']) )$allData =  $objBookTitle->search($_REQUEST);

$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block1 end ##################





######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



################## search  block2 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBookTitle->search($_REQUEST);
    $serial = 1;
}
################## search  block2 end ##################


?>

<?php include_once ('../../../Menu.php');?>


<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css"
    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 900px;
    z-index: -999999; top: 0px;" >

</head>
<style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
        Margin: 25px auto;
        background-color: rgba(11, 42, 68, 0.09);
    }
    td, tr {
        padding: 8px;
        text-align: center;
    }
    th {
        text-align: center;
        padding: 25px;
    }
</style>

<body>


<table>
    <tr>
        <td width="400px">
            <h2>Active List of Book: </h2>
        </td>
        <td width="550px">
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
        </td>
        <td width="200px">
            <form id="searchForm" action="index.php"  method="get">
                <input type="text" value=" " id="searchID" name="search" placeholder="Search" width="60"  >
                <input type="checkbox"  name="byTitle"   checked  >By Title
                <input type="checkbox"  name="byAuthor"  checked >By Author
                <input hidden type="submit" class="btn-primary" value="search">
            </form>
        </td>
    </tr>
</table>

<!-- required for search, block2 end -->


<div id="TopMenuBar" align="center">
    <button type="button" onclick="window.location.href='index.php'" class=" btn-primary btn-lg">Home</button>
    <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
    <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-success btn-lg">Trashed List</button>
</div>



<?php
$serial = 1;

echo "<table > ";
echo "<th> Serial </th> <th> ID </th> <th> Book Title </th> <th> Author Name </th><th> Action </th>";
foreach($someData as $oneData){
    echo "<tr>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->book_title </td>";
    echo "<td> $oneData->author_name </td>";

    echo
        "<td>
          <a href='view.php?id=$oneData->id'><button class='btn btn-info'>View </button></a>
          <a href='edit.php?id=$oneData->id'><button class='btn btn-success'>Edit </button></a>
          <a href='trash.php?id=$oneData->id'><button class='btn btn-primary'>Trash </button></a>
          <a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete </button></a>
        </td>
        ";
    echo "</tr>";
    $serial++;
}
echo "</table>";
?>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="left" class="container">
    <ul class="pagination">

        <?php
        echo '<li><a href="">' . "Previous" . '</a></li>';
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        echo '<li><a href="">' . "Next" . '</a></li>';
        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->



</body>
</html>




<!-- required for search, block5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 end -->
