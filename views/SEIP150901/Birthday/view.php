<?php
require_once("../../../vendor/autoload.php");
use App\Birthday\Birthday;

$objBirthday = new Birthday();

$objBirthday->setData($_GET);
$oneData = $objBirthday->view("obj");


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css"
    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 100%;
    z-index: -999999; top: 0px;" >

</head>

<body>
<div class="container">
    <h2><?php echo $oneData->person_name ?>'s Birthday </h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $oneData->id?> </li>
        <li class="list-group-item">Username: <?php echo $oneData->person_name ?> </li>
        <li class="list-group-item">Birthdate: <?php echo $oneData->birthdate ?> </li>

    </ul>
</div>

</body>
</html>

