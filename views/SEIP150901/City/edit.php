<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();

use App\City\City;

$objCity=new City();
$objCity->setData($_GET);
$oneData=$objCity->view("obj");

?>
<!DOCTYPE html>
<html>
<head>
    <title>City</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_city/css/select_jquery.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include JS File Here -->
    <script src="../../../Resource/assets_city/js/jquery.min.js"></script>
   <script type="text/javascript" src="../../../Resource/assets_city/js/select_jquery.js"></script>

</head>
<body>
<div class="container">

    <div class="main">
        <h2>City</h2>
        <?php
        $fromDB = $oneData->country_name;
        ?>

        <?php
        $fromDB = $oneData->city_name;
        //var_dump($fromDB);
        //die;
        ?>

        <form action="update.php" method="post">
            <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
            <label>Select Country:</label>
            <div id="prm">
                <select id="country" name="country_name" value="" >
                    <option><?php echo $oneData->country_name?></option>
                    <option>--Select--</option>
                    <option>USA</option>
                    <option>AUSTRALIA</option>
                    <option>FRANCE</option>
                    <option>Bangladesh</option>
                </select>
                <br><br>
            </div>
            <label>Select City:</label>
            <div id="prm">
                <select id="city" name="city_name" >
                    <option><?php echo $oneData->city_name?></option>
                </select><br>
            </div>
            <button type="submit" class="btn">Update</button>
        </form>

    </div>
</div>
</body>
</html>
