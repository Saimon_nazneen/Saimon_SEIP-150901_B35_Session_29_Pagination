<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Upload User Profile Picture</title>
    <!-------Including jQuery from Google ------>
    <script src="../../../Resource/assets_propic/js/jquery.min.js"></script>
    <script src="../../../Resource/assets_propic/js/script.js"></script>
    <!------- Including CSS File ------>
    <link rel="stylesheet" type="text/css" href="../../../Resource/assets_propic/css/style.css">
    <link rel="stylesheet" href="../../../Resource/assets_propic/font-awesome/css/font-awesome.min.css">
<body>
<div id="maindiv">

    <div id="formdiv">
        <h2>Image Upload Form</h2>


        <form role="form" action="store.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
            <div class="form"><i class="fa fa-user"></i>
                <label>Username:</label>
                <input type="text" name="name" placeholder="Username..." >
            </div>
            <div class="form-group"><i class="fa fa-file"></i>
                <label>Upload file:</label>
                <input type ="file" name="image" id= "fileToUpload">
            </div>
            </div>
            <button type="submit" class="btn">Upload</button>

        </form>

        <!-------Including PHP Script here ------>

    </div>
</div>
</body>
</html>