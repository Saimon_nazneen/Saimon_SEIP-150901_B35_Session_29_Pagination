<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
use App\Hobbies\Hobbies;

$objHobbies=new Hobbies();
$objHobbies->setData($_GET);
$oneData=$objHobbies->view("obj");


$hobbies_array=explode(",",$oneData->hobbies);
//Utility::dd($hobbies_array);


?>
<html>
<head>
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../Resource/assets_hobbies/css/style.css" />
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <div class="main">
        <h2>Hobbies</h2>
        <form role="form" action="update.php" method="post" class="login-form">
            <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
            <div class="form-group"><i class="fa fa-user"></i>
                <label>Username :</label>
                <input type="text" name="name" id="username" value="<?php echo $oneData->name?>">
            </div>
            <div class="form-group"><i class="fa fa-check"></i>
                <label class="heading">Select Your Hobbies</label>
                <div class="form-group">
                    <input type="checkbox" name="hobbies[]" value="travelling" <?php if(in_array("travelling",$hobbies_array)) : ?> checked <?php endif; ?> <label>Travelling</label></div>
                <div class="form-group">
                    <input type="checkbox" name="hobbies[]" value="Programming" <?php if(in_array("Programming",$hobbies_array)) : ?> checked <?php endif; ?><label>Programming</label></div>
                <div class="form-group">
                    <input type="checkbox" name="hobbies[]" value="Gardening" <?php if(in_array("Gardening",$hobbies_array)) : ?> checked <?php endif; ?><label>Gardening</label></div>
                <div class="form-group">
                    <input type="checkbox" name="hobbies[]" value="Working" <?php if(in_array("Working",$hobbies_array)) : ?> checked <?php endif; ?><label>Working</label></div>
                <div class="form-group">
                    <input type="checkbox" name="hobbies[]" value="Reading" <?php if(in_array("Reading",$hobbies_array)) : ?> checked <?php endif; ?><label>Reading Book</label></div>
                <div class="form-group">
                    <button type="submit" class="btn">Update</button>
                </div>
                <!----- Including PHP Script ----->

        </form>
    </div>
</div>
</body>
</html>
