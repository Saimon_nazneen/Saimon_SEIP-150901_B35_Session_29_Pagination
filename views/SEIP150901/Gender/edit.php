<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();

use App\Gender\Gender;

$objGender=new Gender();
$objGender->setData($_GET);
$oneData=$objGender->view("obj");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Gender</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_gender/css/style.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include JavaScript File Here -->
    <script src="../../../Resource/assets_gender/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../../Resource/assets_gender/js/reset.js"></script>
</head>
<body>
<div class="container">
    <h1>Add Gender</h1>
    <div class="main">
        <?php
            $fromDB = $oneData->sex;

        ?>
        <form role="form" action="update.php" method="post" class="login-form">
            <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
            <div class="form-group"> <i class="fa fa-users"></i>
                <label>Name :</label>
                <input type="text" name="name" value="<?php echo $oneData->name ?> ">
            </div>
            <div class="form-group"> <i class="fa fa-gender"></i>
                <label>Gender :</label>
                <div class="form-group">
                    <input type="radio" name="sex" value="Male" <?php $fromDB==='Male' ? print 'checked' : print "" ?> >Male<br></div>
                <div class="form-group">
                    <input type="radio" name="sex" value="Female" <?php $fromDB==='Female' ? print 'checked' : print "" ?> >Female </div>
            </div><br>
            <div class="form-group">
                <button type="submit" class="btn">Update</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>