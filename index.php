<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Atomic Project</title>
<link href="asset/Bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="asset/Bootstrap/css/styles.css" rel="stylesheet">
<!--Icons-->
<script src="asset/Bootstrap/js/lumino.glyphs.js"></script>
</head>

<body>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><span>Atomic </span>Project</a>
			<ul class="user-menu">
				<li class="dropdown pull-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user">
						<use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div><!-- /.container-fluid -->
  </nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
			
			<li class="active"><a href="index.php"><svg class="glyph stroked dashboard-dial">
				<use xlink:href="#stroked-dashboard-dial"></use></svg> Home</a></li>
			<li><a href="views/SEIP150901/BookTitle/index.php"><svg class="glyph stroked calendar">
				<use xlink:href="#stroked-calendar"></use></svg> BookTitle </a></li>
			<li><a href="views/SEIP150901/Birthday/index.php"><svg class="glyph stroked calendar">
				<use xlink:href="#stroked-calendar"></use></svg> Birthday </a></li>
			<li><a href="views/SEIP150901/City/index.php"><svg class="glyph stroked line-graph">
				<use xlink:href="#stroked-line-graph"></use></svg> City </a></li>
			<li><a href="views/SEIP150901/Email/index.php"><svg class="glyph stroked table">
				<use xlink:href="#stroked-table"></use></svg> Email </a></li>
			<li><a href="views/SEIP150901/Gender/index.php"><svg class="glyph stroked pencil">
				<use xlink:href="#stroked-pencil"></use></svg> Gender </a></li>
			<li><a href="views/SEIP150901/Hobbies/index.php"><svg class="glyph stroked app-window">
				<use xlink:href="#stroked-app-window"></use></svg> Hobby </a></li>
			<li><a href="views/SEIP150901/ProfilePicture/index.php"><svg class="glyph stroked star">
				<use xlink:href="#stroked-star"></use></svg> Profile Picture </a></li>
			<li><a href="views/SEIP150901/SummaryOfOrganization/index.php"><svg class="glyph stroked star">
				<use xlink:href="#stroked-star"></use></svg> Summary Of Organization </a></li>

			<li role="presentation" class="divider"></li>
			<li><a href="login.php"><svg class="glyph stroked male-user">
						<use xlink:href="#stroked-male-user"></use></svg> Login Page
				</a>
			</li>
		</ul>

	</div>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<!--<img src="Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px; border: medium none; width: 98%; height: 100%;
    z-index: -999999; top: 0px;">
			/.row-->

		<table>
			<tr>
				<td height="100">


					<div id="AtomicProjectListMenu">
						<button type="button" onclick="window.location.href='BookTitle/index.php'" class=" btn-success btn-lg">Book Title</button>
						<button type="button" onclick="window.location.href='Birthday/index.php'" class=" btn-primary btn-lg">Birthday</button>

						<button type="button" onclick="window.location.href='City/index.php'" class=" btn-danger btn-lg">City</button>
						<button type="button" onclick="window.location.href='EmailSubscription/index.php'" class=" btn-primary btn-lg">Email Subscription</button>
						<button type="button" onclick="window.location.href='Gender/index.php'" class=" btn-success btn-lg">Gender</button>
						<button type="button" onclick="window.location.href='Hobby/index.php'" class=" btn-danger btn-lg">Hobby</button>
						<button type="button" onclick="window.location.href='ProfilePicture/index.php'" class=" btn-primary btn-lg">Profile Picture</button>
						<button type="button" onclick="window.location.href='SummaryOfOrganization/index.php'" class=" btn-success btn-lg">Summary Of Organization</button>




					</div>
				</td>

			</tr>
		</table>

	</div>	<!--/.main-->

	<script src="asset/Bootstrap/js/jquery-1.11.1.min.js"></script>
	<script src="asset/Bootstrap/js/bootstrap.min.js"></script>

	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>